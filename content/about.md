+++
title = "About Page"
+++

What's the point of this site? I'd say I want to understand things clearly, and explain them well. Except that would be stolen from [Chris Olah's (far superior) blog](https://colah.github.io/about.html).

Basically this is for writing down what I'm learning because I will forget. Except that's also stolen from [Wes Peter's site](https://wespeter.com/)

