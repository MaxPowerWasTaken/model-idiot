+++
author = "Max Epstein"
date = 2016-12-14
title = "Avoiding that Pandas 'SettingWithCopyWarning'"
description = ""
tags = [
    "Pandas",
    "Data-Wrangling",
    "Static-Site-Builders",
    "Blogging-about-Blogging",
]
+++

If you use Python for data analysis, you probably use [Pandas](http://pandas.pydata.org/pandas-docs/stable/) for Data Wrangling. And if you use Pandas, you've probably come across the warning below:

```
SettingWithCopyWarning: 
A value is trying to be set on a copy of a slice from a DataFrame.
Try using .loc[row_indexer,col_indexer] = value instead

See the caveats in the documentation: http://pandas.pydata.org/pandas-docs/stable/indexing.html#indexing-view-versus-copy
```

The Pandas documentation is great in general, but it's easy to read through the link above and still be confused. Or if you're like me, you'll read the documentation page, think "Oh, I get it," and then get the same warning again.

## A Simple Reproducible Example of The Warning<sup>(tm)</sup>

Here's where this issue pops up. Say you have some data:


```python
import pandas as pd
df = pd.DataFrame({'Number' : [100,200,300,400,500], 'Letter' : ['a','b','c', 'd', 'e']})
df
```

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number</th>
      <th>Letter</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>100</td>
      <td>a</td>
    </tr>
    <tr>
      <th>1</th>
      <td>200</td>
      <td>b</td>
    </tr>
    <tr>
      <th>2</th>
      <td>300</td>
      <td>c</td>
    </tr>
    <tr>
      <th>3</th>
      <td>400</td>
      <td>d</td>
    </tr>
    <tr>
      <th>4</th>
      <td>500</td>
      <td>e</td>
    </tr>
  </tbody>
</table>
</div>



...and you want to filter it on some criteria. Pandas makes that easy with [Boolean Indexing](http://pandas.pydata.org/pandas-docs/stable/indexing.html#boolean-indexing)


```python
criteria = df['Number']>300
criteria
```




    0    False
    1    False
    2    False
    3     True
    4     True
    Name: Number, dtype: bool




```python
#Keep only rows which correspond to 'Number'>300 ('True' in the 'criteria' vector above)
df[criteria]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number</th>
      <th>Letter</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>400</td>
      <td>d</td>
    </tr>
    <tr>
      <th>4</th>
      <td>500</td>
      <td>e</td>
    </tr>
  </tbody>
</table>
</div>



This works great right? Unfortunately not, because once we:
1. Use that filtering code to create a new Pandas DataFrame, and
2. Assign a new column or change an existing column in that DataFrame

like so...


```python
#Create a new DataFrame based on filtering criteria
df_2 = df[criteria]

#Assign a new column and print output
df_2['new column'] = 'new value'
df_2
```

    /home/maxepstein/.local/lib/python3.7/site-packages/ipykernel_launcher.py:5: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      """





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number</th>
      <th>Letter</th>
      <th>new column</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>400</td>
      <td>d</td>
      <td>new value</td>
    </tr>
    <tr>
      <th>4</th>
      <td>500</td>
      <td>e</td>
      <td>new value</td>
    </tr>
  </tbody>
</table>
</div>



There's the warning.

So what should we have done differently? The warning suggests using ".loc[row_indexer, col_indexer]". So let's try subsetting the DataFrame the same way as before, but this time using the df.loc[ ] method.

## Re-Creating Our New Dataframe Using .loc[]


```python
df.loc[criteria, :]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number</th>
      <th>Letter</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>400</td>
      <td>d</td>
    </tr>
    <tr>
      <th>4</th>
      <td>500</td>
      <td>e</td>
    </tr>
  </tbody>
</table>
</div>




```python
#Create New DataFrame Based on Filtering Criteria
df_2 = df.loc[criteria, :]

#Add a New Column to the DataFrame
df_2.loc[:, 'new column'] = 'new value'
df_2
```

    /home/maxepstein/.local/lib/python3.7/site-packages/pandas/core/indexing.py:844: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      self.obj[key] = _infer_fill_value(value)
    /home/maxepstein/.local/lib/python3.7/site-packages/pandas/core/indexing.py:965: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      self.obj[item] = s





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number</th>
      <th>Letter</th>
      <th>new column</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>400</td>
      <td>d</td>
      <td>new value</td>
    </tr>
    <tr>
      <th>4</th>
      <td>500</td>
      <td>e</td>
      <td>new value</td>
    </tr>
  </tbody>
</table>
</div>



Two warnings this time!

## OK, So What's Going On?

Recall that our "criteria" variable is a Pandas Series of Boolean True/False values, corresponding to whether a row of 'df' meets our Number>300 criteria.


```python
criteria
```




    0    False
    1    False
    2    False
    3     True
    4     True
    Name: Number, dtype: bool



The Pandas Docs say a ["common operation is the use of boolean vectors to filter the data"](http://pandas.pydata.org/pandas-docs/stable/indexing.html#boolean-indexing) as we've done here. But apparently a boolean vector is not the "row_indexer" the warning advises us to use with .loc[] for creating new dataframes. Instead, Pandas wants us to use .loc[] with a vector of row-numbers (technically, "row labels", which here are numbers).

## Solution

We can get to that "row_indexer" with one extra line of code. Building on what we had before. Instead of creating our new dataframe by filtering rows with a vector of True/False like below...


```python
df_2 = df[criteria]
```

We first grab the indices of that filtered dataframe using .index...


```python
criteria_row_indices = df[criteria].index
criteria_row_indices
```




    Int64Index([3, 4], dtype='int64')



And pass that list of indices to .loc[ ] to create our new dataframe


```python
new_df = df.loc[criteria_row_indices, :]
new_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number</th>
      <th>Letter</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>400</td>
      <td>d</td>
    </tr>
    <tr>
      <th>4</th>
      <td>500</td>
      <td>e</td>
    </tr>
  </tbody>
</table>
</div>



Now we can add a new column without throwing The Warning <sup>(tm)</sup>


```python
new_df['New Column'] = 'New Value'
new_df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Number</th>
      <th>Letter</th>
      <th>New Column</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>400</td>
      <td>d</td>
      <td>New Value</td>
    </tr>
    <tr>
      <th>4</th>
      <td>500</td>
      <td>e</td>
      <td>New Value</td>
    </tr>
  </tbody>
</table>
</div>



## Final Note - Did That Warning Even Mean Our Results Were Wrong?

In each of the instances above where we got a warning, you may have noticed that we also got the results we expected. Maybe the warning isn't such a big deal? It's not an error right? 

The Pandas documentation page linked in the warning states that the results may be correct, but are not reliably correct, because of the unpredictable nature of when an underlying \__getitem\__ call returns a view vs a copy. After reading some StackOverflow discussions, at least one dev is confident that "if you know what you are doing", you can ignore these warnings (or suppress them) and rest assured your results are reliable. 

I'm sure that works for him, but even if I managed to convince myself when it's safe to ignore this warning, what happens in a year when I forget if some old code which throws the warning is reliable or not? Was this written before I figured it out? What happens when someone else is using my code, asks about the warning, and I say "don't worry it's fine, but I forget why" and wave my hands a lot.

Plus, doesn't that warning just bother you? Either out of prudence or neuroticism, I'm not interested in peppering my logs with warnings from the Pandas devs, and I'm not cavalier enough to suppress the warning messages. 

To me, the clean code solution requires using code that provides reliably correct results without these warnings.
