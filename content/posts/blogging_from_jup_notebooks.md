+++
author = "Max Epstein"
date = 2014-09-28
title = "Blogging from Jupyter Notebooks"
description = ""
tags = [
    "Hugo",
    "Web-Development",
    "Static-Site-Builders",
    "Blogging-about-Blogging",
]
draft="true"
+++

[prelude] create post-specific folder in jup-notebooks...for when I eventually get this to work with a venv in each
1. open up jup notebook, add `!pip3 install [package]` for each [package] imported.
2. from terminal...
    - jupyter nbconvert --ExecutePreprocessor.kernel_name='python3' --ExecutePreprocessor.timeout=600 --to markdown --execute mynotebook.ipynb
        - see   https://nbconvert.readthedocs.io/en/latest/execute_api.html
3. move the generated md file from jupyter_notebooks/ to content/posts
4. add front matter
5. move any images in the generated []files/ folder to /static/img/[post-name]
6. update img links in generated md file, along with any other misc edits based on how md renders on locally served site


continue to get fucked and be unable to even get to `import pandas as pd` with a desired python env. next way to try?
    - try opening ipynb in vscode?