+++
title = "Expanding a Pandas DataFrame to have one row by [A] by [B]"
description = ""
tags = [
    "Python",
    "Pandas",
    "Pandas Indexes",
    "MultiIndex",
    "Data Wrangling",
]
date = "2020-04-06"
katex= true
+++

## Motivation
Yesterday I was working on a project where I was given a dataset organized by datetime by user. The dataset came in pre-filtered on some criteria, but to move forward with a particular analysis, I needed to first expand the dataset to include one row per datetime per user. 

This task comes up every once in a while, but yesterday I still spent more time googling & then debugging my initial approach than I was happy with, so I figured I ought to write up how how I do this in Pandas for the next time I forget.

## Our Input Data
First, here's a little toy-dataset I created on the fly (code to reproduce this dataset in an appendix at the end)
```python
df = pd.read_csv('input_data.csv')
df.head()

                    dt user  feature
0  2017-11-21 07:00:00    c        0
1  2016-11-02 10:00:00    b        2
2  2019-05-29 11:00:00    a        2
3  2016-01-28 17:00:00    b        1
4  2018-04-02 16:00:00    b        3
```

## Creating our Comprehensive Index 
The first step was to create a comprehensive date range for all the dates in our timeframe.
```python
df['dt'] = pd.to_datetime(df.dt, format="%Y-%m-%d %H:M:S")
start = df.dt.min()
end = df.dt.max()
dt_range = pd.date_range(start=start, end=end, freq='1H')
```
(Documentation for pd.date_range is [here](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.date_range.html) and for frequency-aliases (e.g. '1D' for daily, '2H' for every other hour, etc, is [here](https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases) )

Our `daterange` object above is not actually a Pandas `Series`, but a `DatetimeIndex`.

The next step is to get our unique set of users, and generate a new (Multi-)Index based on the cartesian product of (all combinations of) our dates and users. Pandas.MultiIndex class has a great method for this, `from_product`:

```python
users = df.user.unique()
multindx = pd.MultiIndex.from_product([dt_range, users])
```
Here's what the first 10 entries in our `dt_range` look like:
```python
mltindx[:10]
MultiIndex([('2015-12-31 00:00:00', 'a'),
            ('2015-12-31 00:00:00', 'c'),
            ('2015-12-31 00:00:00', 'b'),
            ('2015-12-31 01:00:00', 'a'),
            ('2015-12-31 01:00:00', 'c'),
            ('2015-12-31 01:00:00', 'b'),
            ('2015-12-31 02:00:00', 'a'),
            ('2015-12-31 02:00:00', 'c'),
            ('2015-12-31 02:00:00', 'b'),
            ('2015-12-31 03:00:00', 'a')],
           )
```


## Expanding Our Original Data to One Row by Datetime By User
Finally, all we have to do to expand our original dataframe to have one row by datetime by user is:
```python
df = df.set_index(['date', 'user'])
df = df.reindex(index=multindx).sort_index()
```

We can now see that we've expanded our original dataset to contain one row per date per user:
```python
>>> df.head()
                       feature
2015-12-31 00:00:00 a      NaN
                    b      2.0
                    c      NaN
2015-12-31 01:00:00 a      NaN
                    b      NaN
```
And:
```python
df.shape[0] == len(dt_range) * len(users)
True
```

Also, we can see above that our 'feature' column was converted from an Int to a float (the nonmissing value is '2.0' not '2'). This is because our reindex operation creates missing values (`NaN`), for indices that were missing in our original dataset, and `NaN` in Pandas/Numpy is inherently a float value. So inserting `NaN`s into our `feature` column coerces it from int to float.

(Finally just for completeness, below is the code used to generate the initial toy datset):
```python
daterange = pd.date_range(start=pd.Timestamp('12-31-2015 00:00'), end=pd.Timestamp('12-31-2019 00:00'), freq='1H')
df = pd.DataFrame({'user': np.random.choice(['a','b','c'], size=len(daterange)),
                   'feature': np.random.randint(low=0,high=10, size=len(daterange))
                  }, index=daterange).reset_index().rename(columns={'index':'dt'})
df = df.sample(frac = 0.7).reset_index(drop=True)
df.to_csv('input_data.csv', index=False)
```