```python
!pip3 install pandas
!pip3 install matplotlib
!pip3 install scikit-learn
```

    Requirement already satisfied: pandas in /home/maxepstein/.local/lib/python3.7/site-packages (1.0.1)
    Requirement already satisfied: python-dateutil>=2.6.1 in /home/maxepstein/.local/lib/python3.7/site-packages (from pandas) (2.8.1)
    Requirement already satisfied: pytz>=2017.2 in /usr/lib/python3/dist-packages (from pandas) (2019.2)
    Requirement already satisfied: numpy>=1.13.3 in /home/maxepstein/.local/lib/python3.7/site-packages (from pandas) (1.18.1)
    Requirement already satisfied: six>=1.5 in /usr/lib/python3/dist-packages (from python-dateutil>=2.6.1->pandas) (1.12.0)
    Requirement already satisfied: matplotlib in /home/maxepstein/.local/lib/python3.7/site-packages (3.1.3)
    Requirement already satisfied: pyparsing!=2.0.4,!=2.1.2,!=2.1.6,>=2.0.1 in /home/maxepstein/.local/lib/python3.7/site-packages (from matplotlib) (2.4.6)
    Requirement already satisfied: cycler>=0.10 in /home/maxepstein/.local/lib/python3.7/site-packages (from matplotlib) (0.10.0)
    Requirement already satisfied: kiwisolver>=1.0.1 in /home/maxepstein/.local/lib/python3.7/site-packages (from matplotlib) (1.1.0)
    Requirement already satisfied: python-dateutil>=2.1 in /home/maxepstein/.local/lib/python3.7/site-packages (from matplotlib) (2.8.1)
    Requirement already satisfied: numpy>=1.11 in /home/maxepstein/.local/lib/python3.7/site-packages (from matplotlib) (1.18.1)
    Requirement already satisfied: six in /usr/lib/python3/dist-packages (from cycler>=0.10->matplotlib) (1.12.0)
    Requirement already satisfied: setuptools in /usr/lib/python3/dist-packages (from kiwisolver>=1.0.1->matplotlib) (41.1.0)
    Requirement already satisfied: scikit-learn in /home/maxepstein/.local/lib/python3.7/site-packages (0.22.1)
    Requirement already satisfied: scipy>=0.17.0 in /home/maxepstein/.local/lib/python3.7/site-packages (from scikit-learn) (1.4.1)
    Requirement already satisfied: joblib>=0.11 in /home/maxepstein/.local/lib/python3.7/site-packages (from scikit-learn) (0.14.1)
    Requirement already satisfied: numpy>=1.11.0 in /home/maxepstein/.local/lib/python3.7/site-packages (from scikit-learn) (1.18.1)


## Exploring the MNIST Digits Dataset

### Introduction
The MNIST digits dataset is a famous dataset of handwritten digit images. You can read more about it at [wikipedia](https://en.wikipedia.org/wiki/MNIST_database) or [Yann LeCun's page](http://yann.lecun.com/exdb/mnist/). It's a useful dataset because it provides an example of a pretty simple, straightforward image processing task, for which we know exactly what state of the art accuracy is.


I plan to use this dataset for a couple upcoming machine learning blog posts, and since the first step of pretty much any ML task is 'explore your data,' I figured I would post this first, to have to refer back to, instead of repeating in each subsequent post.

Conveniently, `scikit-learn` has a built-in utility for loading this (and other) standard datsets.

### Loading the Digits Dataset


```python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
import os
from sklearn.datasets import fetch_openml
```


```python
X, y = fetch_openml('mnist_784', version=1, return_X_y=True)
```


```python
# Convert from numpy ndarrays to Pandas DataFrames
y = pd.Series(y).astype('int').astype('category')
X = pd.DataFrame(X)
```

## Data Shape, Summary Stats

We can see below that our data (X) and target (y) have 70,000 rows, meaning we have information on 70,000 digit images. Our X, or independent variables dataset, has 784 columns, which correspond to the 784 pixel values in a 28-pixel x 28-pixel image (28x28 = 784). Our y, or target, is a single column representing the true digit labels (0-9) for each image.


```python
X.shape, y.shape
```




    ((70000, 784), (70000,))




```python
# Change column-names in X to reflect that they are pixel values
num_images = X.shape[1]
X.columns = ['pixel_'+str(x) for x in range(num_images)]

# print first row of X
X.head(1)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>pixel_0</th>
      <th>pixel_1</th>
      <th>pixel_2</th>
      <th>pixel_3</th>
      <th>pixel_4</th>
      <th>pixel_5</th>
      <th>pixel_6</th>
      <th>pixel_7</th>
      <th>pixel_8</th>
      <th>pixel_9</th>
      <th>...</th>
      <th>pixel_774</th>
      <th>pixel_775</th>
      <th>pixel_776</th>
      <th>pixel_777</th>
      <th>pixel_778</th>
      <th>pixel_779</th>
      <th>pixel_780</th>
      <th>pixel_781</th>
      <th>pixel_782</th>
      <th>pixel_783</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
<p>1 rows × 784 columns</p>
</div>



Below we see min, max, mean and most-common pixel-intensity values for our rows/images. As suggested by the first row above, our most common value is 0. In fact even the median is 0, which means over half of our pixels are background/blank space. Makes sense.


```python
X_values = pd.Series(X.values.ravel())
print(" min: {}, \n max: {}, \n mean: {}, \n median: {}, \n most common value: {}".format(X_values.min(), 
                                                                                          X_values.max(), 
                                                                                          X_values.mean(),
                                                                                          X_values.median(), 
                                                                                          X_values.value_counts().idxmax()))
```

     min: 0.0, 
     max: 255.0, 
     mean: 33.385964741253645, 
     median: 0.0, 
     most common value: 0.0


We might wonder if there are only a few distinct pixel values present in the data (e.g. black, white, and a few shades of grey), but in fact we have all 256 values between our min-max of 0-255:


```python
len(np.unique(X.values))
```




    256



## Viewing the Digit Images

We can also take a look at the digits images themselves, with matplotlib's handy function `pyplot.imshow`().

`imshow` accepts a dataset to plot, which it will interpret as pixel values. It also accepts a color-mapping to determine the color each pixel-value should be displayed as.

In the code below, we'll plot our first row/image, using the "reverse grayscale" color-map, to plot 0 (background in this dataset) as white.


```python
# First row is first image
first_image = X.loc[0,:]
first_label = y[0]

# 784 columns correspond to 28x28 image
plottable_image = np.reshape(first_image.values, (28, 28))

# Plot the image
plt.imshow(plottable_image, cmap='gray_r')
plt.title('Digit Label: {}'.format(first_label))
plt.show()
```


![png](Exploring%20the%20MNIST%20Digits%20Dataset_files/Exploring%20the%20MNIST%20Digits%20Dataset_17_0.png)


And here's a few more...


```python
images_to_plot = 9
random_indices = random.sample(range(70000), images_to_plot)

sample_images = X.loc[random_indices, :]
sample_labels = y.loc[random_indices]
```


```python
plt.clf()
plt.style.use('seaborn-muted')

fig, axes = plt.subplots(3,3, 
                         figsize=(5,5),
                         sharex=True, sharey=True,
                         subplot_kw=dict(adjustable='box', aspect='equal')) #https://stackoverflow.com/q/44703433/1870832

for i in range(images_to_plot):
    
    # axes (subplot) objects are stored in 2d array, accessed with axes[row,col]
    subplot_row = i//3 
    subplot_col = i%3  
    ax = axes[subplot_row, subplot_col]

    # plot image on subplot
    plottable_image = np.reshape(sample_images.iloc[i,:].values, (28,28))
    ax.imshow(plottable_image, cmap='gray_r')
    
    ax.set_title('Digit Label: {}'.format(sample_labels.iloc[i]))
    ax.set_xbound([0,28])

plt.tight_layout()
plt.show()
```


    <Figure size 432x288 with 0 Axes>



![png](Exploring%20the%20MNIST%20Digits%20Dataset_files/Exploring%20the%20MNIST%20Digits%20Dataset_20_1.png)


## Final Wrap-up

One last thing I'd want to check here before moving forward with any classification task, would be to determine how balanced our dataset is. Do we have a pretty even distribution of each digit? Or do we have mostly 7s, for example?


```python
y.value_counts(normalize=True)
```




    1    0.112529
    7    0.104186
    3    0.102014
    2    0.099857
    9    0.099400
    0    0.098614
    6    0.098229
    8    0.097500
    4    0.097486
    5    0.090186
    dtype: float64



Great. Almost all the digits appear in between 9.7% and 10.4% of our rows. 11.3% of our digits are `1`, which is the most common, and `5` is the least common at 9.0%. Overall a very well-balanced dataset. 
