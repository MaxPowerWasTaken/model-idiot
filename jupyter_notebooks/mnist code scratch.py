#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 00:52:20 2017

@author: max
"""


import math
import random
import numpy as np
import pandas as pd
from sklearn.datasets import fetch_mldata
from sklearn.cross_validation import train_test_split
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import os
os.chdir('/home/max/model_idiot/content/jupyter_notebooks')



# load mnist data
mnist = fetch_mldata('MNIST original', data_home='datasets/')

# Convert sklearn 'datasets bunch' object to Pandas
y = pd.Series(mnist.target).astype('int').astype('category')
X = pd.DataFrame(mnist.data)

# Change column-names in X to reflect that they are pixel values
X.columns = ['pixel_'+str(x) for x in range(X.shape[1])]



# Prepare to plot 9 random images
images_to_plot = 9
random_indices = random.sample(range(70000), images_to_plot)

sample_images = X.loc[random_indices, :]
sample_labels = y.loc[random_indices]


X_train, X_test, y_train, y_test = train_test_split(X,y, test_size = .4)

from sklearn.pipeline import make_pipeline
from sklearn.decomposition import PCA
tsne = TSNE()
tsne

# It is highly recommended to use another dimensionality reduction method (e.g. PCA for dense data or 
# TruncatedSVD for sparse data) to reduce the number of dimensions to a reasonable amount (e.g. 50) 
# if the number of features is very high.
rows=1000
sample_indices = random.sample(range(X_train.shape[0]), rows)
X_train_sample = X_train.iloc[sample_indices,:]
y_train_sample = y_train.iloc[sample_indices]

# https://www.reddit.com/r/MachineLearning/comments/47kf7w/scikitlearn_tsne_implementation/ (suggests lr=200)
pca_preprocessed_tsne = make_pipeline(PCA(n_components=50), TSNE(n_components=2, learning_rate=200, perplexity=50))
embedded_data = pca_preprocessed_tsne.fit_transform(X_train_sample)




plt.figure()
ax = plt.subplot(111)
X = embedded_data
for i in range(X.shape[0]):
    print(i)
    print(X[i, 0], X[i, 1])
    print(str(y_train_sample.iloc[i]))
    print(y_train_sample.iloc[i])
    plt.text(X[i, 0], X[i, 1], str(y_train_sample.iloc[i]),
             color=plt.cm.Set1(y_train_sample.iloc[i] / 10.),
             fontdict={'weight': 'bold', 'size': 9})

plt.show()







