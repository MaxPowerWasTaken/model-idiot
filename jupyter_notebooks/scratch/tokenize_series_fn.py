
import pandas as pd
import re
from nltk.corpus import stopwords
from datetime import datetime

def tokenize_series(text_series):
    ''' Accept a series of strings, list of words (lowercased) without punctuation or stopwords'''

    # lowercase everything
    text_series = text_series.str.lower()
    
    # remove punctuation (r'\W' is regex, matches any non-alphanumeric character)
    text_series = text_series.str.replace(r'\W', ' ')
    
    # return list of words, without stopwords
    sw = stopwords.words('english')
    #words_list = text_series.str.split()

    return text_series.apply(lambda row: [word for word in row.split() if word not in sw])


# Download questions dataset
df = pd.read_csv('../datasets/quora_kaggle.csv')

# Time function call
start = datetime.now()
df['q1_tokenized'] = tokenize_series(df['question1'])

print('Time elapsed: ', datetime.now() - start)
print(df[['question1', 'q1_tokenized']].head(3))
