import pandas as pd
import re
from nltk.corpus import stopwords
from datetime import datetime

def tokenize_str(text):
    ''' Accept a string, list of words (lowercased) without punctuation or stopwords'''
    # lowercase everything
    text = text.lower()
    
    # remove punctuation (\W matches any non-alphanumeric character)
    text = re.sub("\W", " ", text)
    
    # return list of words, without stopwords
    droplist = stopwords.words('english')
    
    return [word for word in text.split() if word not in droplist]

# Download questions dataset
df = pd.read_csv('../datasets/quora_kaggle.csv')

# Time function call
start = datetime.now()
df['q1_tokenized'] = df['question1'].apply(tokenize_str)

print('Time elapsed: ', datetime.now() - start)
print(df[['question1', 'q1_tokenized']].head(5))
